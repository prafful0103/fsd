package com.lockedme.application;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class UserService {

	Scanner scannerObject = new Scanner(System.in);
	LoginService loginService = new LoginService();

	public void userRegistration(String fileName) throws IOException {
		System.out.print("Please enter Username :: ");
		String username = scannerObject.next();
		System.out.print("Please enter Password :: ");
		String password = scannerObject.next();
		System.out.print("Please enter Date of Birth :: ");
		String dateOfBirth = scannerObject.next();

		String content = username + "," + password + "," + dateOfBirth;
		try (FileWriter fw = new FileWriter(fileName, true)) {
			fw.write(content + "\n");
		}

		System.out.println("User is registered successfully.");
//		switchOption();
	}

	public void userLogin(String fileName, String lockerFileName) {

		System.out.print("Please enter Username :: ");
		String username = scannerObject.next();
		System.out.print("Please enter Password :: ");
		String password = scannerObject.next();

		Boolean[] isLoginSuccess = { false };
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> {
				String data[] = line.split(",");
				if (username.contentEquals(data[0]) && password.contentEquals(data[1])) {
					System.out.println("Login is Successful.");
					isLoginSuccess[0] = true;
					System.out.println("Choose any one of the below option : ");
					System.out.println("1. Store credentials.");
					System.out.println("2. Retrive credentials.");
					System.out.print("Enter your option : ");
					int option = scannerObject.nextInt();
					switch (option) {
					case 1:
						try {
							loginService.saveCredentials(username, lockerFileName);
						} catch (IOException e) {
							e.printStackTrace();
						}
						break;
					case 2:
						try {
							loginService.retriveCredentials(username, lockerFileName);
						} catch (IOException e) {
							e.printStackTrace();
						}
						break;
					default:
						System.out.println("Please select either 1 or 2");
						break;
					}
				}
			});
			if (isLoginSuccess[0].booleanValue() != true) {
				System.out.println("Invalid username/password.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
package com.lockedme.application;

import java.io.IOException;
import java.util.Scanner;

public class RegistrationDemo {
	static String fileName = "database.txt";
	static String lockerFileName = "locker-file.txt";
	static Scanner scannerObject = new Scanner(System.in);

	UserService userService = new UserService();

	public static void main(String[] args) throws IOException {
		RegistrationDemo registrationDemo = new RegistrationDemo();
		registrationDemo.initiateTheProcess();
		scannerObject.close();
	}

	public void initiateTheProcess() throws IOException {

		System.out.println("Welcome to Lockeme.com....");
		System.out.println("Choose any one of the below option : ");
		System.out.println("1. Registration");
		System.out.println("2. Login");
		System.out.println("3. Exit");
		System.out.print("Enter your option : ");
		int option = scannerObject.nextInt();
		switch (option) {
		case 1:
			userService.userRegistration(fileName);
			break;
		case 2:
			userService.userLogin(fileName, lockerFileName);
			break;
		default:
			System.out.println("Please select either 1 or 2");
			break;
		}
	}

}